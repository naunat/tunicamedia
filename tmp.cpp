#define _USE_MATH_DEFINES
#define OMP
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cassert>
#include "tissue.h"


using namespace std;


void tissue::set_all_forces()
{


    double const lmax_c=1.27, hertz_x=0.43*0.02, lmax_x=1.10, hertz_c = (1.4+3*0.1)*1.52*16*0.02,
        wmlc_x=0.00000007*(0.7+0.1*1)*1.46*0.02, wmlc_c = (0.00000025+0.00000002*3)*0.9*16*0.02;

    int numcells = cell_array.size();

//#ifdef OMP
   omp_set_num_threads(2);
//#endif
#pragma omp parallel
    {
        int m;
        double x,y,z,x1,y1,z1,rad,rad1,frad,fbx,fby,fbz;

#pragma omp for

    for (int i=0;i<numcells;i++) {cell_array[i].get_external_forces(fbx,fby,fbz);
                                  cell_array[i].set_forces(fbx,fby,fbz);
                                  cell_array[i].set_sn(abs(fbx),abs(fby),abs(fbz));}

#pragma omp for

    for (int i=0;i<numcells;i++)

    {
        double fx=0,fy=0,fz=0,sx=0,sy=0,sz=0;
        cell *thiscell = &cell_array[i];
        thiscell->getpos(x,y,z);
        thiscell->get_forces(fx,fy,fz);
        thiscell->get_sn(sx,sy,sz);

        rad = thiscell->getrad();
        int nit = thiscell->num_nb;

        for(int j=0;j<nit;j++)
        {
        m = thiscell->nbrs[j];
        if (m<=i) continue;
        cell *thatcell = &cell_array[m];
        thatcell->getpos(x1,y1,z1);
        double fmx,fmy,fmz;
        thatcell->get_forces(fmx,fmy,fmz);
        double smx,smy,smz;
        thatcell->get_sn(smx,smy,smz);
        rad1 = thatcell->getrad();

        double dist = sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1)+
                (z-z1)*(z-z1));
        if (dist==0) continue;
        double identdepth = dist-rad-rad1;
        const double invr = 1.0 / dist;
        const double cosfi = thiscell->springdir[j]; //angle between spring and circumferential direction

         //wmlc+hertz model

          const double lmax = (rad+rad1)*(lmax_x+(lmax_c-lmax_x)*cosfi);
          double zz=dist/lmax;
          double kbT=wmlc_x+(wmlc_c-wmlc_x)*cosfi;
          double hertz_coeff = hertz_x - (hertz_x-hertz_c)*cosfi;

          double popravka=0.036/mean_rad;

          if (identdepth<=0) {
              const double Rad = rad*rad1/(rad+rad1);//1.0/(1.0/R1 + 1.0/R2); //average Radius of the sphere
              const double Rsqr = Rad*Rad;
              const double asqr = Rad*(-identdepth);
              const double a = sqrt(asqr); //contact radius, taken from indentation depth via sqrt(R*identdepth) or identdepth^0.498 * R^(0.503-3.97e-6*indetdepth),see ref Lin et al Spherical identation of soft matter

              // we could also try to measure contact radius directly as it is done in isr2d
              const double acub = a*asqr;
              const double afou = asqr*asqr;
              const double afif = acub*asqr;
              const double znam = (4.0*a-3.0*M_PI*Rad)*(4.0*a-3.0*M_PI*Rad);
              const double firstpart = (16.0*afif - 36.0*M_PI*Rad*afou + 27.0*M_PI*M_PI*Rsqr*acub) / znam ;
              const double B = 0.0282385035;//1.0/8.3439;//0.5*19.59; //kPa  //B in terms of young, looking for efficiency // B=E/6/(1-nu*nu)
             // const double B=0.5*19.59;
              frad = - 8.0*B * firstpart/ 3.0 / Rad ;
              frad = frad* hertz_coeff;
              }
          else {frad=0.;}

          double fwmlc=(zz<0.99)?(kbT/lmax/(popravka*popravka*popravka)*(6.0*zz+(3.0*zz*zz-2.0*zz*zz*zz)/(1-zz)/(1-zz))):(kbT*10000.0/lmax/(popravka*popravka*popravka));

          frad+=fwmlc;

          //end of wmlc+hertz model    */

        //x-,y-, z-projections of force
        double deltafx = frad*invr*(x1-x);
        double deltafy = frad*invr*(y1-y);
        double deltafz = frad*invr*(z1-z);
        fx+= deltafx; fmx+= -deltafx;
        fy+= deltafy; fmy+= -deltafy;
        fz+= deltafz; fmz+= -deltafz;

        sx+=frad*invr*abs(x1-x);
        smx+=frad*invr*abs(x1-x);
        sy+=frad*invr*abs(y-y1);
        smy+=frad*invr*abs(y-y1);
        sz+=frad*invr*abs(z-z1);
        smz+=frad*invr*abs(z-z1);

        thatcell->set_forces(fmx,fmy,fmz);
#pragma omp flush
        thatcell->set_sn(smx,smy,smz);
#pragma omp flush

        }   //end of neighbour cycle

        thiscell->set_forces(fx,fy,fz);
#pragma omp flush
        thiscell->set_sn(sx,sy,sz);
#pragma omp flush

    }  //end of cell cycle

    } // end of parallel section

    return ;
}


void tissue::CheckConnectivities()
{
    int numcells=cell_array.size();
    double x,y,z,x1,y1,z1,rad,rad1;
    int n;

    for (int i=0;i<numcells;i++)
    {
        n=0;
        cell me = cell_array[i];
        me.getpos(x,y,z);
        rad = me.getrad();

        for(int j=i+1;j<numcells;j++)
        {
            cell nb = cell_array[j];
            nb.getpos(x1,y1,z1);
            rad1 = nb.getrad();
            const double dist = sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1)+(z-z1)*(z-z1));
            if (dist==0) continue;
            const double identdepth = dist-rad-rad1;
            if(identdepth<DTOL) {
                cell_array[i].num_nb++;
                int nbi=cell_array[i].num_nb;
                cell_array[j].num_nb++;
                int nbj=cell_array[j].num_nb;
                cell_array[i].nbrs[nbi-1]=j;
                cell_array[j].nbrs[nbj-1]=i;

                // Store springs directions
                const double cosfi = abs(z1-z)/dist; //angle between spring and circumferential direction
                cell_array[i].springdir[nbi-1] = cosfi;
                cell_array[j].springdir[nbj-1] = cosfi;

            }
        }
    }
}


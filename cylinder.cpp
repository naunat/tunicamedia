#include "cylinder.h"
#include <cassert>
#include <cmath>
#define _USE_MATH_DEFINES
#define M_PI 3.14159265358979323846

cylinder::cylinder(){}
cylinder::cylinder(double rad, double lX, double r1, double r2)
{
    assert(rad>0 && lX>0);
    assert(r1>=0 && r2>=0);
    assert(r2 > r1);

    mean_rad=rad; LX = lX, IR=r1, OR=r2;

    double s = 2*mean_rad;

    // approximate dimensions of a cuboid to be transformed into the cylindrical shell;
    // exact dimensions must be computed as (nJ-1)*dY, (nK-1)*dZ.
    const double lY = (r2-r1);
    const double lZ = 2*M_PI * 0.5*(r1+r2);

    // computations as for the drh cuboid
    const double dZ = s;
    const double dY = s * 0.5*sqrt(3.0);
    const double dX = s * sqrt(2.0/3.0);

    int nI = (int)ceil(lX/dX);
    if (nI % 2 == 0) { nI=nI-1; }   // ensure an odd number of layers
    int nJ = (int)ceil(lY/dY);
    if (nJ % 2 == 0) { nJ=nJ-1; }   // ensure an odd number of rows
    int nK = (int)ceil(lZ/dZ);
    if (nK % 2 == 0) { nK=nK-1; }   // ensure an odd number of columns


    // generate a regular drh cuboid;
    // code replication from the method above, except that agents in plane z=0 are not generated.
    axiflag=0;
    int n = 0;
    double x, y, z;

    for (int i=0; i<nI; i++) {

        x = i*dX;
        const int layerParity = (i % 2);
        const double y0 = (layerParity == 0) ? 0.0 : 0.5*dZ/sqrt(3.0);

        for (int j=0; j<nJ; j++) {

            y = y0 + j*dY;
            const int maxK = (j % 2 == layerParity) ? nK : nK-1;
            const double z0 = (j % 2 == layerParity) ? 0.0 : 0.5*dZ;
            celltype type_id = (j==(nJ-1))? EEL : SMC; //upper layer contains EEL cells

            for (int k=0; k<maxK; k++) {
                n++;
                z = z0 + k*dZ;
                if (z > 0.0) { cell_array.push_back(cell(type_id ,mean_rad*(1+0*(lY-y)/lY), x, y, z) ); }
            }
        }
    }

     //this->FileOutput("../output/tube_opening_angle.csv");

    //Check connectivities and add periodicity

    this->CheckConnectivities();

    //COMMENT 23 JUNE:
    this->AddZPeriodicBC(0.5*dZ, dZ*(nK-1));

    // transform cuboid into a cylindrical shell by wrapping it around the x-axis.

    // exact middle and inner radius of the wrapping transformation;
    // these may differ from 0.5*(r1+r1) resp. r1 because exact domain dimensions can differ from lY,lZ.
    axiflag=1;

    const double rMean = (nK-1)*dZ / (2.0*M_PI);
    const double rOffs = rMean - 0.5*(nJ-1)*dY;

    int n_cells = cell_array.size();
    for (int i=0;i<n_cells;i++)
    {
        double x,y,z;
        cell_array[i].getpos(x,y,z);

        double phi = z / rMean;
        double yPrime = (y+rOffs) * cos(phi);
        double zPrime = (y+rOffs) * sin(phi);

        cell_array[i].setpos(x,yPrime,zPrime);
    }
    std::cout << "Total number of cells is "<<cell_array.size()<<std::endl;
}

void cylinder::SelectBoundaryForStent(double strutlength, double strutwidth, int numstruts, std::vector<int> & selection)
{
    selection.clear();
    std::vector<int> seltmp;
    double x1 = (LX-strutlength)/2.0;
    double x2 = x1 + strutlength;

    const double deltar = OR-IR;
    const double dY = 2*mean_rad * 0.5*sqrt(3.0);
    int nJ = (int)ceil(deltar/dY);
    if (nJ % 2 == 0) { nJ=nJ-1; }
    int nK = (int)ceil((2*M_PI * 0.5*(OR+IR))/(2*mean_rad));
    if (nK % 2 == 0) { nK=nK-1; }
    const double rMean = (nK-1)*2*mean_rad / (2.0*M_PI);
    const double r_inner = rMean - 0.5*(nJ-1)*dY;

    this->SelectBoundary('R',r_inner, seltmp);

  for(int i = 0; i<seltmp.size(); i++)
  {
      double x,y,z;
      int it = seltmp[i];
      cell_array[it].getpos(x,y,z);

      if ((x>=x1)&&(x<=x2))
      {      //const double phi = asin(z/(sqrt(y*y+z*z)+0.0000001));
             for (int j=0;j<numstruts;j++)
             {
                 const double phistart = 2*M_PI/numstruts*j;
                 const double phiend = phistart+strutwidth*M_PI/180.0;
                 const double phi = (z>=0)?acos(y/sqrt(z*z+y*y)):(2*M_PI-acos(y/sqrt(z*z+y*y)));
                 //cout << "phistart = "<< phistart<<endl;
                         if ((phi>phistart)&&(phi<phiend)) selection.push_back(it);//break;
             }
      }
  }
  return;
}

void cylinder::SelectBoundaryFor3DStent(double strutlength, double strutwidth, int numstruts,
                                        double l0, double l1, double l2, double l3, std::vector<int> & selection)
{
    selection.clear();
    std::vector<int> seltmp;
    double x1 = (LX-strutlength)/2.0;
    double x2 = x1 + strutlength;
    const double deltastrut = strutwidth*M_PI/180.0;

    const double deltar = OR-IR;
    const double dY = 2*mean_rad * 0.5*sqrt(3.0);
    int nJ = (int)ceil(deltar/dY);
    if (nJ % 2 == 0) { nJ=nJ-1; }
    int nK = (int)ceil((2*M_PI * 0.5*(OR+IR))/(2*mean_rad));
    if (nK % 2 == 0) { nK=nK-1; }
    const double rMean = (nK-1)*2*mean_rad / (2.0*M_PI);
    const double r_inner = rMean - 0.5*(nJ-1)*dY;

    const double tanalpha = l0/(2*M_PI*r_inner/numstruts);

    this->SelectBoundary('R',r_inner, seltmp);

  for(int i = 0; i<seltmp.size(); i++)
  {
      double x,y,z;
      int it = seltmp[i];
      cell_array[it].getpos(x,y,z);

      if ((x>=x1)&&(x<=(x1+l3)))
      {
             for (int j=0;j<numstruts;j++)
             {
                 const double phistart = 2*M_PI/numstruts*j;
                 const double phiend = phistart+deltastrut;
                 const double phi = (z>=0)?acos(y/sqrt(z*z+y*y)):(2*M_PI-acos(y/sqrt(z*z+y*y)));
                 const double fi1start = //phistart+M_PI/2.0*(x-x1)/r_inner/tanalpha - deltastrut/2.0;
                                         phistart+M_PI*(x-x1)/tanalpha/(2.0*M_PI*r_inner);
                                        // 2*pi*R_inner/dl; dl = (x-x1)/tanalpha
                 const double fi1end = fi1start + deltastrut;
                 const double fi2start = 2*M_PI/numstruts*(j+1) - M_PI*(x-x1)/tanalpha/(2.0*M_PI*r_inner);
                         //2*M_PI/numstruts*(j+1) - M_PI/2.0*(x-x1)/r_inner/tanalpha - deltastrut/2.0;


                 const double fi2end = fi2start + deltastrut;
                 const double fimid = (fi1start+fi2start)/2.0 - deltastrut/2.0;

                if ((x-x1)<l0)
                {
                if ((phi>phistart && phi<phiend)||(phi>fi1start && phi<fi1end)||(phi>fi2start && phi<fi2end )||
                        (phi>fimid && phi< (fimid + deltastrut))) {selection.push_back(it);/*cell_array[it].setrad(mean_rad*1.1);*/}
                }
                else if (((x-x1)>=l0)&&((x-x1)<l1))
                {
                if (phi>phistart && phi<phiend) {selection.push_back(it);/*cell_array[it].setrad(mean_rad*1.1);*/}
                }
                else

                if (((x-x1)>=l1) && ((x-x1)<=l2))
                {
                if ((phi>fi1start && phi<fi1end)||(phi>fi2start && phi<fi2end)) {selection.push_back(it);
                   /* cell_array[it].setrad(mean_rad*1.1);*/}
                }
                else if (((x-x1)>l2)&&(x<x2)){
                if (phi>fimid && phi< (fimid + deltastrut)) {selection.push_back(it);/*cell_array[it].setrad(mean_rad*1.1);*/}
                }

            }
      }
  }
}

cylinder::~cylinder()
{

}


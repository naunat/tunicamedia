#ifndef CYLINDER_H
#define CYLINDER_H
#include "tissue.h"


class cylinder : public tissue
{
    double LX, IR, OR;
public:
    cylinder();
    cylinder(double mean_rad, double lX, double r1, double r2);
    void SelectBoundaryForStent(double stentlength, double, int, std::vector<int> & selection);
    void cylinder::SelectBoundaryFor3DStent(double strutlength, double strutwidth, int numstruts,
                                            double l0, double l1, double l2, double l3, std::vector<int> & selection);
    ~cylinder();
};

#endif // CYLINDER_H

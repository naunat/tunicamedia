#ifndef TISSUE_H
#define TISSUE_H

#include "cell.h"
#include <iostream>
#include "omp.h"

using namespace std;


class tissue
{
protected:
    bool axiflag;
    double DTOL, mean_rad;
    double lmax_c, lmax_x, hertz_c, hertz_x, wmlc_c, wmlc_x;
public:
    vector<cell> cell_array;
    tissue();
    inline void ChangeRheology(double const lmax_c_, double const lmax_x_, double const hertz_c_, double const hertz_x_,
                            double const wmlc_c_, double const wmlc_x_)
    {
        lmax_c=lmax_c_; lmax_x = lmax_x_; hertz_c = hertz_c_, hertz_x = hertz_x_,
                    wmlc_c = wmlc_c_, wmlc_x = wmlc_x_;
    }
    double set_all_forces();
    void CheckConnectivities();
    void AddZPeriodicBC(double z1, double z2);
    void SelectBoundary(char locdir, double locvalue, std::vector<int> & selection);
    void FixBoundary(const std::vector<int> & selection, bool *mobility);
    void ApplyForceToBoundary(const std::vector<int> & selection, double *forces);
    void MoveBoundary(const std::vector<int> & selection, double *v);
    void GetBoundaryResponse(const std::vector<int> & selection, double & frx, double & fry, double & frz);
    void Set_initial_condition(double *v);
    double DoStep(double);
    double DoExpStep(double);
    double DoCGStep(double *, double *,double &, double &, int const);
    double DoRK4Step(double);
    void FileOutput(std::string filename);
    void FileInput(std::string filename);
    void Linesearch(double *, const double);
    void Testsearch(double *, const double);
    ~tissue();
};

#endif // TISSUE_H

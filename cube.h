#ifndef CUBE_H
#define CUBE_H

#include "tissue.h"


class cube : public tissue
{
public:
    cube();
    cube(double mean_rad, double lX, double lY, double lZ);
    ~cube();
};

#endif // CUBE_H

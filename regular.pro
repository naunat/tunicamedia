TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS+= -openmp
QMAKE_LFLAGS +=  -openmp

SOURCES += main.cpp \
    cell.cpp \
    tissue.cpp \
    cube.cpp \
    cylinder.cpp \
    dotubetest.cpp \
    docubictest.cpp \
    cailbrate.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    cell.h \
    tissue.h \
    cube.h \
    cylinder.h \
    refdata.h


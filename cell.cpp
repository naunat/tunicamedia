#include "cell.h"
#include <vector>

cell::cell(celltype type_id_,double r, double x, double y, double z)
{
    type_id = type_id_;
    num_nb=0;
    rad = r;
    pos[0]=x; pos[1]=y; pos[2]=z;
    f[0] = f[1] = f[2] = 0;
    fb[0]= fb[1]= fb[2]=0;
    sn[0] = sn[1] = sn[2] = 0;
    mobilities[0] = mobilities[1]= mobilities[2] = 1; // by default all cells are mobile
}


cell::~cell()
{

}


#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <ctime>
#include "cube.h"
#include "refdata.h"


using namespace std;

void Equillibrate(int const MAX_ITER, double const EQTOL, double timestep, tissue & sample,
                  const int);
string MakeFileName(string str1, double time, string str2);

void DoCubicTest(double timestep, double const mean_rad, double const EQTOL, int const MAX_ITER, bool const contiflag, const int method,
                 char const loadaxis,
                 double const lmax_c_,double const lmax_x_,double const hertz_c_,
                 double const hertz_x_,double const wmlc_c_,double const wmlc_x_, double * result)
{

    std::vector<int> selection, selection3, selection_z,selection3_z,selection_y,selection3_y;
    double frx,fry,frz,deformed_lX, deformed_lY, deformed_lZ;

        if (loadaxis=='X') cout << "Performing axial tension test"<<endl;
        else if (loadaxis=='Z') cout << "Performing circumferential tension test"<<endl;
        else {cout << "ERROR, WRONG LOADAXIS";return ;}

    //double lX=7.21, lY=0.32,lZ=2.81;  //Holzapfel's paper dimensions, cube tension
    double lX=7.21/1.5, lY=0.32,lZ=2.81/2.0;    //test of adhesion model
    //double lX=7.21/12.0, lY=0.32,lZ=2.81/4.0; //test of different radii //was: /8, /2
     //double lX=0.037*5, lY=0.10,lZ=0.10;        //5-cell model
    //double lX=7.21/2.0, lY=0.32/2.5,lZ=0.32/2.5;      //test chain model  was:/2.5
    //double lX=mean_rad, lY=27*mean_rad,lZ=lY;    //cube shear

    cube sample(mean_rad, lX, lY, lZ);
    sample.ChangeRheology(lmax_c_,lmax_x_,hertz_c_,hertz_x_,wmlc_c_,wmlc_x_);
    if (!contiflag) sample.FileOutput("../output/cuboid_stage_1.csv");

    const double dZ = 2*mean_rad;
    //const double dY = mean_rad*sqrt(3.0);        // "row spacing"
    const double dX = mean_rad*2*sqrt(2.0/3.0);  // "layer spacing"
    const int nI = (int)floor(lX/dX);
    //const int nJ = (int)floor(lY/dY);
    const int nK = (int)floor(lZ/dZ);

    sample.SelectBoundary('X', 0., selection); //select boundary to fix

    sample.SelectBoundary('X', (nI-1)*dX  , selection3); //select boundary to move

    // 1.7048448610 3.5272652296        0.1175755077    - for rad 0.036
   // sample.SelectBoundary('X', 0.4115142768  , selection2);
    //1.7636326148 0.8818163074  0.0587877538

    //bool mob0[3]={0,0,0};
    //sample.FixBoundary(selection, mob0);

    sample.SelectBoundary('Z', 0., selection_z); //select boundary to fix
    sample.SelectBoundary('Z', (nK-1)*dZ , selection3_z); //select boundary to move

    sample.SelectBoundary('Y',0.,selection_y);
    sample.SelectBoundary('Y',0.2494153163,selection3_y);

    clock_t start = clock();

    if (contiflag) {sample.FileInput("../output/cuboid_stage_2.csv");
    cout<<"Continuation of simulation"<<endl;}
    else {
    Equillibrate(MAX_ITER, EQTOL, timestep, sample, method); // 1st stage: free of loads equillibration
    cout << "Time taken to equilibrate  " << (clock()-start) << endl;
    sample.FileOutput("../output/cuboid_stage_2.csv");
    }

    // COMMENT TO MEASURE OPENING_ANGLE IN AXIAL DIRECTION


    // measure domain size in equillibrium configuration:
    double x0,y0,z0,xx,yy,zz;

    sample.cell_array[selection3[selection3.size()/2]].getpos(x0,y0,z0);
    sample.cell_array[selection[selection.size()/2]].getpos(xx,yy,zz);
    double real_lX=x0-xx;

    sample.cell_array[selection3_y[selection3_y.size()/2-nK-1]].getpos(x0,y0,z0);
    sample.cell_array[selection_y[selection_y.size()/2-nK-1]].getpos(xx,yy,zz);
    double real_lY=y0-yy; //0.32;

    sample.cell_array[selection3_z[selection3_z.size()/2]].getpos(x0,y0,z0);
    sample.cell_array[selection_z[selection_z.size()/2]].getpos(xx,yy,zz);
    double real_lZ=z0-zz;

    cout <<  "real_lX ="<< real_lX << " , real lZ = "<< real_lZ <<" , real lY = "<< real_lY <<endl;

    for (int i=selection3_y.size()/2-nK-1;i<selection3_y.size()/2-nK;i++) {double x0,y0,z0;
    sample.cell_array[selection3_y[i]].getpos(x0,y0,z0);cout << x0<< " , "<< z0 << endl;}
    //double tmp; cin >> tmp;

    bool mob[3]={0,0,0};

    int numstages;
    double deltau;

    if (loadaxis=='X') {
    sample.FixBoundary(selection, mob); // axial test
    sample.FixBoundary(selection3, mob); //axial test
    numstages=19;             //  axial test
    deltau=0.38*real_lX;    //  axial test
    }
    else if (loadaxis=='Z'){

    sample.FixBoundary(selection_z, mob); // circum test
    sample.FixBoundary(selection3_z, mob); // circum test
    numstages=17;              //circum test
    deltau=0.34*real_lZ;    //circum test
    }


    double stagestep=deltau/numstages;


    //double movevec[3] = {0,0,deltau/((double)nstages)};
    //double movevec[3] = {0,0,deltau/((double)nstages)};           //shear

    double s=0.;

    string addstr(1,loadaxis);
    string fname="../output/cubicloadcurve";
    fname+=addstr; fname+=".csv";

    ofstream myFile(fname, ios_base::out);
    myFile<<" stretch   ,    stress [KPa],       nu"<< loadaxis <<"y,       nu" << loadaxis << "2     ,     volume change"<<endl;


    cout<< "OK #1"<<endl;

    start=clock();   //omp_get_wtime();

//for (int i=0;i<1;i++)
    for (int i=0;i<numstages;i++) // COMMENT TO MEASURE EFFICIENCY
    {
        s+=stagestep;

        //if (s>deltau) break;
        if (loadaxis=='X') {
            double movevec[3] = {stagestep,0,0};   //axial stretch
            sample.MoveBoundary(selection3, movevec); //axial stretch
            Equillibrate(MAX_ITER, EQTOL, timestep, sample, method);
            sample.GetBoundaryResponse(selection3, frx, fry, frz);
            sample.cell_array[selection3_z[selection3_z.size()/2]].getpos(x0,y0,z0);
            sample.cell_array[selection_z[selection_z.size()/2]].getpos(xx,yy,zz);
            deformed_lZ=z0-zz;
            sample.cell_array[selection3_y[selection3_y.size()/2-nK-1]].getpos(x0,y0,z0);
            sample.cell_array[selection_y[selection_y.size()/2-nK-1]].getpos(xx,yy,zz);
            deformed_lY=y0-yy;
            double srx = -frx/real_lY/real_lZ*1000.0;               // SHOULD CHECK WITH deformed_lY !!!
            cout<<"deformation= "<< (s/real_lX)<< "  srx, KPa = "<< srx <<"  fry= "<<-fry<<"  frz= "<<-frz<<endl;
            myFile<<(1.0+s/real_lX)<<" , "<< srx << " , " << (deformed_lY)/real_lY << " , " <<
                    deformed_lZ/real_lZ << " , "<<(1.0+s/real_lX)*deformed_lY/real_lY*deformed_lZ/real_lZ<<endl;
            result[i] = srx;

        }
        else if (loadaxis=='Z'){
            double movevec[3] = {0,0,stagestep};    //circum stretch
            sample.MoveBoundary(selection3_z, movevec); //circum stretch
            Equillibrate(MAX_ITER, EQTOL, timestep, sample, method);
            sample.GetBoundaryResponse(selection3_z, frx, fry, frz);
            sample.cell_array[selection3[selection3.size()/2]].getpos(x0,y0,z0);
            sample.cell_array[selection[selection.size()/2]].getpos(xx,yy,zz);
            deformed_lX=x0-xx;
            sample.cell_array[selection3_y[selection3_y.size()/2-nK-1]].getpos(x0,y0,z0);
            sample.cell_array[selection_y[selection_y.size()/2-nK-1]].getpos(xx,yy,zz);
            deformed_lY=y0-yy;
            double srz = -frz/real_lY/real_lX*1000.0;
            cout<<"deformation= "<< (s/real_lZ)<< "  srz, KPa = "<< srz <<"  fry= "<<-fry<<"  frx= "<<-frx<<endl;
            myFile<<(1.0+s/real_lZ)<<" ,"<< srz << " , " << deformed_lY/real_lY << " , " <<
                   deformed_lX/real_lX<< " , " << (1.0+s/real_lZ)*deformed_lY/real_lY*deformed_lX/real_lX << endl;
            result[i] = srz;

        }

        //double anothervec[3] = {s/2.0/real_lX,0,0};
        //sample.Set_initial_condition(anothervec);

       cout<<"time taken for 1 load step "<<clock()-start<<endl;

       //string fname=MakeFileName("../output/cubic_stage_"+string(1,loadaxis), (double)(i+1),".csv");
       //if ((!(i%1))||(s==deltau))
       //sample.FileOutput(fname);

        //double deltaf=sqrt((frx-f0x)*(frx-f0x)+(fry-f0y)*(fry-f0y)+(frz-f0z)*(frz-f0z));
        //if ((i>2)&&(deltaf>deltaf0)) {//stagestep*=deltaf0/deltaf;
        //    timestep*=deltaf0/deltaf;}
        //}
        //f0x=frx;f0y=fry;f0z=frz;
       // deltaf0=deltaf;

    }
    myFile.close();

    cout<< "OK ##1"<<endl;    // */  //ENDOF COMMENT TO MEASURE OPENING_ANGLE IN AXIAL DIRECTION

    return;

}

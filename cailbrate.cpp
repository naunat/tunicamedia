#include <iostream>
#include <fstream>
#include "refdata.h"

using namespace std;

void DoCubicTest(double , double const, double const, int const,bool const, int const, char const,
                 double const, double const, double const, double const, double const, double const, double*);

double FindCorrectionCoefficient(double *data_x, double *data_z, double & err)
{

    //cout << "check" <<endl; for (int j=0;j<15;j++) cout << s9fi[j+1] << " , "<<s9x[j+1]<<endl;

    double corr, corrmin=1.0, errmin = 1000000000;

    for (int i=1; i<=10000; i++)
    {
        err = 0.0;
        corr=0.01*i;
        for (int j=0;j<17;j++) {err =err + (data_x[j]*corr-s9x[j+1])*(data_x[j]*corr-s9x[j+1])/s9x[19]/s9x[19]
        + (data_z[j]*corr-s9fi[j+1])*(data_z[j]*corr-s9fi[j+1])/s9fi[17]/s9fi[17];}
        err = err + (data_x[17]*corr-s9x[18])*(data_x[17]*corr-s9x[18])/s9x[19]/s9x[19]+
            + (data_x[18]*corr-s9x[19])*(data_x[18]*corr-s9x[19])/s9x[19]/s9x[19];

        if (err<errmin) {errmin=err;corrmin = corr;}
    }
    err = errmin;
    return corrmin;
}


void Calibrate (double const timestep,double const mean_rad, double const EQTOL, int const MAX_ITER, int const method)
{

    double loadcurve_z[17], loadcurve_x[19];
    double err;

    ofstream myFile_sfi("../output/calibrated_loadcurve_sfi.csv", ios_base::out);
    ofstream myFile_sx("../output/calibrated_loadcurve_sx.csv", ios_base::out);
    ofstream myFile_err("../output/errors.csv", ios_base::out);

        myFile_sfi << "STRETCH , ";
        for (int k=0;k<17;k++) myFile_sfi << stretchfi[k+1] << " ," ;
        myFile_sfi << endl;
        myFile_sfi << "HOLZAPFEL , ";
        for (int k=0;k<17;k++) myFile_sfi << s9fi[k+1] << " ," ;
        myFile_sfi << endl;

        myFile_sx << "STRETCH , ";
        for (int k=0;k<19;k++) myFile_sx  << stretchx[k+1] << " ," ;
        myFile_sx << endl;
        myFile_sx << "HOLZAPFEL , ";
        for (int k=0;k<19;k++) myFile_sx   << s9x[k+1] << " ," ;
        myFile_sx << endl;
   for (int msearch=2;msearch<3;msearch++)
   for (int lsearch=0;lsearch<5;lsearch++)
   for (int ksearch = 3; ksearch<4;ksearch++)
   {
   for (int jsearch=0; jsearch<1;jsearch++)
   {

       double const lmax_x=1.15+0.01*jsearch;

    for (int isearch=0;isearch < 1; isearch ++)
    {
        double const lmax_c=1.20+0.01*isearch, hertz_x=0.45*0.8*1.46; //0.3+0.1*ksearch;//
                double const wmlc_x=0.00000007*(0.7+0.1*msearch)*1.46, wmlc_c = (9.2e-8)*(2.5+0.25*lsearch);
        double const hertz_c = 1.41912;//hertz_x*(2.4+ksearch*0.1);

        bool contiflag = 0;

        DoCubicTest(timestep,mean_rad, EQTOL,MAX_ITER,contiflag, method, 'Z',
                              lmax_c, lmax_x, hertz_c, hertz_x, wmlc_c,wmlc_x,loadcurve_z);
        err=0;


        contiflag = 1;

        DoCubicTest(timestep,mean_rad, EQTOL,MAX_ITER,contiflag, method, 'X',
                              lmax_c, lmax_x, hertz_c, hertz_x, wmlc_c,wmlc_x,loadcurve_x);

        double corr = 1.0; //FindCorrectionCoefficient(loadcurve_x, loadcurve_z, err);

        cout << "lmax_x = "<<lmax_x<< " lmax_c = "<<lmax_c<<" ksearch = "<<ksearch<<"  lsearch = "<<lsearch << endl;
        cout << "CURRENT CORRECTION COEFFICIENT  "<< corr << endl;
        cout << "CURRENT ERROR  " << sqrt(err) << endl;

        myFile_err << "lmax_c=" << lmax_c << "  lmax_x=" << lmax_x<<"  ksearch="<< ksearch <<"  lsearch ="<<lsearch<<" msearch="<<msearch<<" corr="<< corr << " err="<<sqrt(err)<<endl;

        myFile_sfi << "lmax_c=" << lmax_c << "  lmax_x=" << lmax_x<<"  ksearch= "<< ksearch <<"  lsearch= "<<lsearch << ",";
        for (int k=0;k<17;k++) myFile_sfi << loadcurve_z[k]*corr << " ," ;
        myFile_sfi << endl;

        myFile_sx << "lmax_c=" << lmax_c << "  lmax_x=" << lmax_x<<"  ksearch= "<< ksearch<< "  lsearch= "<<lsearch <<",";
        for (int k=0;k<19;k++) myFile_sx  << loadcurve_x[k]*corr << " ," ;
        myFile_sx << endl;
   }
   }
   }
   myFile_sfi.close();
   myFile_sx.close();
   myFile_err.close();

    /*
    double k1=0.7, kbT=k1*0.00000010;
    //cout << "lmax = "<< lmax/(rad+rad1)<<endl;
    //double lmax=1.07*(rad+rad1);     //axial   =1.07

    const double lmax_correction = 1.0+0.1*cosfi; // circum
    const double hertz_coeff = 0.45-0.05*cosfi;
    //0.4 + 0.4*cosfi; //0.225 + 0.575*cosfi correction coefficient for Hertz
                                               //force depending on direction
                                              //AXIAL 0.4, CIRCUM 0.8  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

 /*



    for (int j=0;j<17;j++) myFile << (1.02+j*0.02) << " , "<< loadcurve_z[j]<< " , " << loadcurve_x[j] << endl;

    for (int j=17;j<18;j++) myFile << (1.02+j*0.02) << " ,     -      , "<< loadcurve_x[j] << endl;

    myFile.close();*/


    return;
}

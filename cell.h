#ifndef CELL_H
#define CELL_H
#include <vector>

enum celltype{SMC=0, EEL, STENT};

class cell
{
    double rad, pos[3],f[3],fb[3],hes[3],sn[3],potential;
    celltype type_id;
    //double hes[6];
    bool mobilities[3];
public:
    int num_nb;
    int nbrs[12];
    double springdir[12];
    cell() {}
    cell(celltype type_id_,double r,double x,double y,double z);
    inline void GetMobility(bool & mob0, bool & mob1, bool & mob2)
    {   mob0=mobilities[0];
        mob1=mobilities[1];
        mob2=mobilities[2];}
    inline void SetMobility (bool mob0, bool mob1, bool mob2)
    {   mobilities[0] = mob0;
        mobilities[1]=mob1;
        mobilities[2]=mob2;}
    inline void setpos(double x, double y, double z)    {       pos[0]=x; pos[1]=y; pos[2]=z;    }
    inline void movpos(double x, double y, double z)    {       pos[0]+=x; pos[1]+=y; pos[2]+=z; }
    inline double getrad() { return rad;}
    inline void getpos(double &x, double &y, double &z){ x=pos[0]; y=pos[1]; z=pos[2];}
    inline void setrad(double r) { rad=r;}
    inline void set_forces(double fx, double fy, double fz) { f[0]=fx; f[1]=fy; f[2]=fz;}
    inline void apply_external_forces(double fbx, double fby, double fbz) { fb[0]=fbx; fb[1]=fby; fb[2]=fbz;}
    inline void get_forces(double &fx, double &fy, double &fz) { fx =f[0];  fy=f[1];  fz=f[2];}
    inline void get_external_forces(double &fbx, double &fby, double &fbz) { fbx =fb[0];  fby=fb[1];  fbz=fb[2];}
    inline void set_Hessian(double fxx,double fxy,double fxz,double fyy,double fyz,double fzz)
    {hes[0]=fxx; hes[1]=fxy;hes[2]=fxz;hes[3]=fyy;hes[4]=fyz;hes[5]=fzz;}
    inline void get_Hessian(double & fxx,double & fxy,double & fxz,double & fyy,double & fyz,double & fzz)
    {fxx=hes[0]; fxy=hes[1]; fxz=hes[2]; fyy=hes[3]; fyz=hes[4]; fzz=hes[5];}
    inline void set_sn(double sx,double sy,double sz) {sn[0]=sx;sn[1]=sy;sn[2]=sz;}
    inline void get_sn(double &sx,double &sy,double &sz) {sx=sn[0];sy=sn[1];sz=sn[2];}
    inline celltype gettype(){return type_id;}
    inline void settype(celltype type_id_){type_id=type_id_;}
    ~cell();
};

#endif // CELL_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "cylinder.h"
#define M_PI 3.14159265358979323846

using namespace std;

void Equillibrate(int const MAX_ITER, double const EQTOL, double timestep, tissue & sample,  const int);
string MakeFileName(string str1, double time, string str2);

void DoTubeTest(double timestep, double const mean_rad, double const EQTOL, int const MAX_ITER, bool const contiflag,
                int const method, double const lmax_c_,double const lmax_x_,double const hertz_c_,
                double const hertz_x_,double const wmlc_c_,double const wmlc_x_ )
{
    std::vector<int> sel_longitud, sel_cross, sel_r_inner, sel_stent;
    //double lX_cyl=4*mean_rad, innerradius=2.0,outerradius=innerradius+0.32;
     double lX_cyl=10*mean_rad, innerradius=1.4,outerradius=innerradius+0.31;
     //double lX_cyl=60*mean_rad, innerradius=1.4,outerradius=innerradius+0.31;
    //double lX_cyl=6*mean_rad, innerradius=1.0,outerradius=innerradius+4*mean_rad;

    // Длина 1 мм, диаметр 2.8 мм, толщина стенки 7 слоев, 0.18-0.19, кол-во распорок 6, толщина
    // распорки
    double frx,fry,frz;

    cylinder sample(mean_rad, lX_cyl, innerradius, outerradius);
    sample.ChangeRheology(lmax_c_,lmax_x_,hertz_c_,hertz_x_,wmlc_c_,wmlc_x_);
    sample.set_all_forces();

    if (!contiflag) sample.FileOutput("../output/tube_stage_t_0.csv");

    const double deltar = (outerradius-innerradius);
    const double dY = 2*mean_rad * 0.5*sqrt(3.0);
    int nJ = (int)ceil(deltar/dY);
    if (nJ % 2 == 0) { nJ=nJ-1; }
    int nK = (int)ceil((2*M_PI * 0.5*(outerradius+innerradius))/(2*mean_rad));
    if (nK % 2 == 0) { nK=nK-1; }
    const double rMean = (nK-1)*2*mean_rad / (2.0*M_PI);
    const double r_inner = rMean - 0.5*(nJ-1)*dY;
    const double r_outer = rMean - 0.5*(nJ-1)*dY;

    sample.SelectBoundary('Y', r_outer, sel_longitud); //select boundary to fix

    cout << "sel longit size= "<<sel_longitud.size()<<endl;

    sample.SelectBoundary('X', 0., sel_cross); //select boundary to fix   
        cout << "sel cross size= "<<sel_cross.size()<<endl;
    bool mob[3]={1,1,0};   //ADDED 23 JUNE
    sample.FixBoundary(sel_cross, mob); // fix the boundary in X-direction  // ADDED 23 JUNE

    sample.SelectBoundary('R', r_inner, sel_r_inner); //select boundary to move
        cout << "sel inner size= "<<sel_r_inner.size()<<endl;
    sample.SelectBoundaryForStent(lX_cyl, 0.15/(M_PI*innerradius)*180, 6, sel_stent);
                                               // 0.1/(M_PI*innerradius)*180
    //sample.SelectBoundaryFor3DStent(lX_cyl*0.9, 2.5*mean_rad*180/M_PI, 6,
                                                //lX_cyl*0.9/4.0, lX_cyl*0.9/2.0, lX_cyl*0.9*0.75, lX_cyl*0.9, sel_stent);
    cout << "sel stent size= "<<sel_stent.size()<<endl;


    // TEMPORARY

    //for (int i=0;i<sel_stent.size();i++) sample.cell_array[sel_stent[i]].setrad(mean_rad*1.1);
    //sample.FileOutput("../output/tube_stage_2.csv");

    //ENDOF TEMPORARY



    //bool mob3[3]={0,0,0};
    //sample.FixBoundary(sel_longitud, mob3); // fix rigid motion of the tube

    if (!contiflag) {

    Equillibrate(MAX_ITER, EQTOL, timestep, sample, method); // 1st stage: free of loads equillibration
     sample.FileOutput("../output/tube_stage_2.csv");
    }     //end of stage 1}
    else {
     //stage 2:
         sample.FileInput("../output/tube_stage_2.csv");
         cout<<"Continuation of simulation"<<endl;}

  // COMMENT FROM 23 JUNE 2016

    //bool mob[3]={1,1,0};
    //sample.FixBoundary(sel_cross, mob); // fix the boundary in X-direction
    //bool mob3[3]={0,0,1};
    //sample.FixBoundary(sel_longitud, mob3); // fix rigid motion of the tube
    bool mob2[3]={0,1,1};
    sample.FixBoundary(sel_stent, mob2);
//    sample.FixBoundary(sel_r_inner, mob2);   // fix inner radius


    double deltau=0.1;//0.4; //0.1*innerradius;
    int nstages=10;
    int noutstages=1;

    double movevec[3] = {deltau/nstages,0,0};
    //double smallforce = 0.01;

    double s=0.;
    ofstream myFile("../output/tube_loadcurve.csv", ios_base::out);
    myFile<<" deformation   ,    stress,       nuxy,       nuxz' "<<endl;

    //measure Poisson's ratio:
    //double x0,y0,z0,xx,yy,zz;
    //sample.SelectBoundary('R', outerradius, 0.015, selection2);
    //sample.cell_array[selection2[0]].getpos(x0,y0,z0);

    double f0x=0, f0y=0, f0z=0, deltaf0=1;

    for (int i=0;i<nstages;i++)
    {
        sample.MoveBoundary(sel_stent, movevec); //move one boundary
        //double forcevec[3] = {smallforce*(i+1),0,0};
        //sample.ApplyForceToBoundary(sel_stent, forcevec);

        Equillibrate(MAX_ITER, EQTOL, timestep, sample, method);

        sample.GetBoundaryResponse(sel_r_inner, frx, fry, frz);
        s=s+deltau/((double)nstages);

        //sample.cell_array[selection2[0]].getpos(xx,yy,zz);

        cout<<"displacement= "<< s<< "  frx = "<<frx<<"  fry= "<<fry<<"  frz= "<<frz<<endl;
        //myFile<<s/lX<<" , "<<(-frx/(lY*lZ))<<" ,"<<(abs(y0-yy)/lY)/s*lX<<"  ,  "
             //<< (abs(z0-zz)/lZ)/s*lX <<endl;

        if (!(i%noutstages)){
        myFile<<(s)<<" ,"<<(-frx)<<endl;
        string fname=MakeFileName("../output/tube_stage", (double)(i/noutstages+1),".csv");
        sample.FileOutput(fname);}

        double deltaf=sqrt((frx-f0x)*(frx-f0x)+(fry-f0y)*(fry-f0y)+(frz-f0z)*(frz-f0z));
        if ((i>2)&&(deltaf>deltaf0)) {//stagestep*=deltaf0/deltaf;
            timestep*=deltaf0/deltaf;}
        //}
        f0x=frx;f0y=fry;f0z=frz;
        deltaf0=deltaf;
    }
    myFile.close();
     // END COMMENT FROM 23 JUNE 2016 */



}

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include "cell.h"
#include "cube.h"
#include "cylinder.h"
//#define OMP

using namespace std;
void Equillibrate(int const MAX_ITER, double EQTOL, double timestep, tissue & sample, bool, const int);
void DoCubicTest(double , double const, double const, int const,bool const, int const, char const,
                 double const, double const, double const, double const, double const, double const, double*);
void DoTubeTest(double const timestep, double const mean_rad, double const EQTOL, int const MAX_ITER,bool const, int const,
                double const lmax_c_,double const lmax_x_,double const hertz_c_,
                                double const hertz_x_,double const wmlc_c_,double const wmlc_x_);
void Calibrate (double const timestep,double const mean_rad, double const EQTOL,
                int const MAX_ITER, int const method);
void FileRead(string filename, vector<cell> &cell_array);
string MakeFileName(string str1, double time, string str2);

int main(int argc, char *argv[])
{
    bool contiflag=0;
    int method;
    double timestep=0;

    //if ( argc != 3 ) { cout<<"Wrong number of command line parameters, halt"; return 0;}

    stringstream converter;
    converter<<argv[1];
    converter>>contiflag;
    stringstream converter2;
    converter2<<argv[2];
    converter2>>method;  // 0 - Euler, 1 - descent, 2 - CG

    if (method==2) cout << "USING CONJUGATE GRADIENTS" << endl;
    else if (method==1) cout << "USING FASTEST DESCENT" << endl;
    else {   cout << "USING EULER INTEGRATION, enter time step value" << endl;
             cin >> timestep;
         }

    double const mean_rad = 0.036;
    double const EQTOL=0.2e-8;
    int const MAX_ITER=1000000000;



    //Calibrate(timestep, mean_rad, EQTOL, MAX_ITER, method);

    double const lmax_c=1.27, hertz_x=0.43*0.02, lmax_x=1.10, hertz_c = (1.4+3*0.1)*1.52*16*0.02,
        wmlc_x=0.00000007*(0.7+0.1*1)*1.46*0.02, wmlc_c = (0.00000025+0.00000002*3)*0.9*16*0.02;

    //double const lmax_c=1.18, lmax_x=1.13, hertz_c=(1.4+3*0.1)*0.69, hertz_x=0.5*0.69, wmlc_x=(0.00000007*(0.7+0.1*1)*1.46)*0.69,
        //wmlc_c = (0.00000025+0.00000002*4)*0.69;

    double loadcurve_z[17], loadcurve_x[19];
    ofstream myFile_sfi("../output/sensitivity_loadcurve_sfi.csv", ios_base::out);
    ofstream myFile_sx("../output/sensitivity_loadcurve_sx.csv", ios_base::out);

    for (int ii=-2;ii<3;ii++)
    {
    double lmax_ctemp = lmax_c + 0.01*ii;

    DoCubicTest(timestep,mean_rad, EQTOL,MAX_ITER,contiflag, method, 'Z',lmax_ctemp, lmax_x, hertz_c, hertz_x, wmlc_c,wmlc_x,loadcurve_z);
    contiflag = 1;
    DoCubicTest(timestep,mean_rad, EQTOL,MAX_ITER,contiflag, method, 'X',lmax_ctemp, lmax_x, hertz_c, hertz_x, wmlc_c,wmlc_x,loadcurve_x);

    myFile_sfi << "lmax_c=" << lmax_ctemp
              // << "  lmax_x=" << lmax_x<<"   hertz_c= "<<  hertz_c <<"  hertz_x= "<<hertz_x
              //<< " wmlc_c=" << wmlc_c << " wmlc_x=" << wmlc_x
              << ",";
    for (int k=0;k<17;k++) myFile_sfi << loadcurve_z[k] << " ," ;
    myFile_sfi << endl;

    myFile_sx << "lmax_c=" << lmax_ctemp
              //<< "  lmax_x=" << lmax_x<<"   hertz_c= "<<  hertz_c <<"  hertz_x= "<<hertz_x
              //<< " wmlc_c=" << wmlc_c << " wmlc_x=" << wmlc_x
              << ",";
    for (int k=0;k<19;k++) myFile_sx  << loadcurve_x[k] << " ," ;
    myFile_sx << endl;
    }

    myFile_sfi.close();
    myFile_sx.close();
    //DoTubeTest(timestep,mean_rad, EQTOL,MAX_ITER,contiflag, method,lmax_c, lmax_x, hertz_c, hertz_x, wmlc_c,wmlc_x);

    cout << "Program completed" << endl;
    return 0;
}


void Equillibrate(int MAX_ITER, double EQTOL, double timestep, tissue & sample,
                  const int method)
{
    double err;
    int check=1000;

    int numcells=sample.cell_array.size();
        double* s=new double[numcells*3];
        double* f=new double[numcells*3];
        double betta=0.0,sumold=1.0;
        double err0=1e14;


    for (int i=0;i<MAX_ITER;i++)
    {
        if (method) {err = sample.DoCGStep(s,f,betta,sumold,method);


             //if (err>(1.5*err0)) {betta=0;sumold=1.0;}
             err0=err;
            //EQTOL=0.5e-9; //tolerance necessary for opening angle check
              EQTOL=0.2e-7;  //  0.2e-7
              MAX_ITER=   100000*numcells;
              if (!(i%(2*numcells))) {betta=0;sumold=1.0;}
        }

        else {err = sample.DoStep(timestep);check=100000;//timestep*=1.01;
        }

        if (err<EQTOL) {
            cout << " REACHED equillibrating iterations tolerance " << err <<
                               "  NIT = "<<i<<endl;
                        break;}

            if(((i+1)%check)==0)
                cout << "equillibrating iterations tolerance " << err <<
                                    "  NIT = "<<i+1<<endl;

            if (i==MAX_ITER-1) {cout<< "ERROR!!!! reached max number of iterations without convergence"<<endl;
                cout << "betta = "<<betta<<endl;}
        /*    if(((i+1)%check)==0) {   stringstream converter3;
                converter3 << (i+1)/check;
                sample.FileOutput("../output/tube_stage_t_"+converter3.str() +".csv");}*/

    }



    //cout<<"hi";

    delete [] s;
    delete [] f;
}



    void ConnectivityOutput(string filename)
    {

    /*int firstj, lastj, numcells=indices.size();
    ofstream myFile(filename, ios_base::out);
    if (myFile.is_open())
    {

        firstj=0;

    for (int i=0; i<numcells; i++)
    {
    lastj = indices[i];
    for (int j=firstj;j<lastj;j++) myFile << connectivities[j]<<"   ";
    myFile<<endl;
    firstj=lastj;

    }
       myFile.close();

    }*/
    }

 string MakeFileName(string str1, double time, string str2)
 {
     stringstream converter;
     converter<<time;
     string add;
     converter>>add;

     string filename=str1;
     filename+="_t_";
     filename+= add;
     filename+= str2;
     return (filename);
 }

 void FileRead(string filename, std::vector<cell> &cell_array)
 {

     double x=0, y=0, z=0, rad=0;
     char s;
     int id;
     ifstream myFile(filename, ios_base::in);

     if (myFile.is_open())
     {
     string headings;
     getline(myFile, headings);

     while (!myFile.eof())
     {
     myFile>>x >> s>>y >> s>>z >>s>> rad >> s>> id;

     cell_array.push_back(cell((celltype)id, rad, x, y, z));
     }

     myFile.close();
     }
 }

 double round(double ref){
     double dist1 = abs(floor(ref)-ref);
     double dist2 = abs(ceil(ref)-ref);
     double result;

     if(dist1<dist2){
         result=floor(ref);
     }
     else if (dist1==dist2){
         result = ref>0?(ceil(ref)):(floor(ref));
     }
     else { // if(dist1>dist2)
         result = ceil(ref);
     };
     return(result);
 }

#define _USE_MATH_DEFINES
#define OMP
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cassert>
#include "tissue.h"

double fi1(double z)
{
    if (z==0) return 0;
    else return((exp(z)-1.0)/z);
}


using namespace std;

tissue::tissue(){DTOL=1e-5;
                //this->SetRheology(1.07, 1.07, 0.4, 0.4, 0.00000007, 0.00000007);
                }

double tissue::set_all_forces()
{
    int numcells = cell_array.size();
    double global_force_norm=0, globalU=0;

//#ifdef OMP
   omp_set_num_threads(2);
//#endif
#pragma omp parallel
    {
        int m;
        double x,y,z,x1,y1,z1,rad,rad1,frad,frad_Neo, force_norm=0,t=0,Urad=0,fbx,fby,fbz;
        //cout << "Hi from thread number "<<omp_get_thread_num()<<endl;

#pragma omp for

    for (int i=0;i<numcells;i++) {cell_array[i].get_external_forces(fbx,fby,fbz);
                                  cell_array[i].set_forces(fbx,fby,fbz);
                                  cell_array[i].set_sn(abs(fbx),abs(fby),abs(fbz));}

#pragma omp for

    for (int i=0;i<numcells;i++)
    //for (std::vector<cell>::iterator it = cell_array.begin() ; it != cell_array.end(); ++it)
    {
        double fx=0,fy=0,fz=0,sx=0,sy=0,sz=0;
        cell *thiscell = &cell_array[i];
        //double fxx,fxy,fxz,fyz,fzz,fyy;
        thiscell->getpos(x,y,z);
        thiscell->get_forces(fx,fy,fz);
        thiscell->get_sn(sx,sy,sz);

        rad = thiscell->getrad();
        celltype mytype=thiscell->gettype();

        //cout << "mytype = "<< mytype<<endl;
        int nit = thiscell->num_nb;

        for(int j=0;j<nit;j++)
        {
        m = thiscell->nbrs[j];
        if (m<=i) continue;
        cell *thatcell = &cell_array[m];
        celltype nbtype=thatcell->gettype();
        thatcell->getpos(x1,y1,z1);
        double fmx,fmy,fmz;
        thatcell->get_forces(fmx,fmy,fmz);
        double smx,smy,smz;
        thatcell->get_sn(smx,smy,smz);
        rad1 = thatcell->getrad();

        double dist = sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1)+
                (z-z1)*(z-z1));
        if (dist==0) continue;
        double identdepth = dist-rad-rad1;
        const double invr = 1.0 / dist;
        const double cosfi = thiscell->springdir[j]; //angle between spring and circumferential direction
        //cout << "cosfi = "<<cosfi<<endl;


   /*            //check linear force
        if ((mytype==1)&&(nbtype==1)) {frad = (0.01*identdepth+0.00022);
        Urad+=(0.01*identdepth*identdepth/2.0+0.00022*identdepth)+0.00000242;}
        else {
        frad = 0.01*identdepth+0.0002;
        //const double df = 0.01;
        Urad+=0.01*identdepth*identdepth/2.0+0.0002*identdepth+0.000002;
        } //  */

/*    // hertzian+linear_spring model

        if (identdepth<=0) {
            const double Rad = rad*rad1/(rad+rad1);//1.0/(1.0/R1 + 1.0/R2); //average Radius of the sphere
            const double Rsqr = Rad*Rad;
            //const double Rcub = Rsqr*Rad;

            const double asqr = Rad*(-identdepth);
            const double a = sqrt(asqr); //contact radius, taken from identation depth via sqrt(R*identdepth) or identdepth^0.498 * R^(0.503-3.97e-6*indetdepth),see ref Lin et al Spherical identation of soft matter

            // we could also try to measure contact radius directly as it is done in isr2d
            const double acub = a*asqr;
            const double afou = asqr*asqr;
            const double afif = acub*asqr;
            const double firstpart = (16.0*afif - 36.0*M_PI*Rad*afou + 27.0*M_PI*M_PI*Rsqr*acub) / (4.0*a-3.0*M_PI*Rad)/(4.0*a-3.0*M_PI*Rad) ;
            const double B = 0.0282385035;//1.0/8.3439;//0.5*19.59; //kPa  //B in terms of young, looking for efficiency // B=E/6/(1-nu*nu)
            //const double B=0.5*19.59;
            frad = - 8.0*B * firstpart/ 3.0 / Rad ;
        }
        else {
            //frad = 0.01*identdepth;
            //calibrate to Holzapfel:
            frad=0.0005*identdepth;
            }
     // end of hertzian+linear_spring model    */

/*        //hertzian+adhesive model


        if (identdepth<=0) {
            const double Rad = rad*rad1/(rad+rad1);//1.0/(1.0/R1 + 1.0/R2); //average Radius of the sphere
            const double Rsqr = Rad*Rad;
            //const double Rcub = Rsqr*Rad;

            const double asqr = Rad*(-identdepth);
            const double a = sqrt(asqr); //contact radius, taken from identation depth via sqrt(R*identdepth) or identdepth^0.498 * R^(0.503-3.97e-6*indetdepth),see ref Lin et al Spherical identation of soft matter

            // we could also try to measure contact radius directly as it is done in isr2d
            const double acub = a*asqr;
            const double afou = asqr*asqr;
            const double afif = acub*asqr;
            const double firstpart = (16.0*afif - 36.0*M_PI*Rad*afou + 27.0*M_PI*M_PI*Rsqr*acub) / (4.0*a-3.0*M_PI*Rad)/(4.0*a-3.0*M_PI*Rad) ;
            const double B = 0.0282385035;//1.0/8.3439;//0.5*19.59; //kPa  //B in terms of young, looking for efficiency // B=E/6/(1-nu*nu)
            //const double B=0.5*19.59;
            frad = - 8.0*B * firstpart/ 3.0 / Rad ;
            //fadhesive = 0.00004 + 0.0075*(-identdepth);
            double fadhesive=0.000144 + pow(5.5*(-identdepth), 4.5);
            frad= frad + fadhesive;
        }
        else {
            //frad = 0.01*identdepth;
            //frad=0.0001*identdepth;
            frad=0.00001;
            }

*/        //endof hertzian+adhesive model

/*        //wmlc+pow model

        double lmax=1.05*(rad+rad1);
        double zz=dist/lmax;
        double k1=0.7, k2=k1*0.0000001, kbT=k1*0.00000010;
        frad=(-k2/(dist+0.0001)/(dist+0.0001)); //repulsive force
        Urad+=k2/(dist+0.0001);

        //double fwmlc=(zz<0.99)?kbT/lmax*(6.0*zz+(3.0*zz*zz-2.0*zz*zz*zz)/(1-zz)/(1-zz))
                             //:(kbT/lmax*(2.0e6)*(zz-0.99)+kbT/lmax*10000.0);
        double fwmlc=(zz<0.99)?kbT/lmax*(6.0*zz+(3.0*zz*zz-2.0*zz*zz*zz)/(1-zz)/(1-zz)):kbT*10000.0/lmax;
        Urad+=(zz<0.99)?kbT*(3.0*zz*zz-2.0*zz*zz*zz)/(1.0-zz):kbT*10000.0*zz;
        //Urad+=(zz<0.99)?kbT*(3.0*zz*zz-2.0*zz*zz*zz)/(1.0-zz):kbT*(2.0e6*(zz*zz/2.0-0.99*zz)+10000.0*zz);


        frad+=fwmlc;
        //double dfrad=2*k2/(dist+0.0001)/(dist+0.0001)/(dist+0.0001);
        //dfrad=frad+(zz<0.99)?(kbT/lmax/lmax*(6.0+(6.0*zz-6*zz*zz)/(1.0-zz)/(1.0-zz)+(6*zz*zz-4*zz*zz*zz)/(1.0-zz)/(1.0-zz)/(1.0-zz))):24.684;


        //end of wmlc+pow model  */

         //wmlc+hertz model

          const double lmax = (rad+rad1)*(lmax_x+(lmax_c-lmax_x)*cosfi);//; // circum
          double zz=dist/lmax;
          double kbT=wmlc_x+(wmlc_c-wmlc_x)*cosfi;
          //double ceel=0.01;
          //if ((mytype==EEL)&&(nbtype==EEL))
            // kbT = kbT*5.5;


              //kbT*=10.05; //mimicing EEL's higher stiffness
          double popravka=0.036/mean_rad;

          if (identdepth<=0) {
              const double Rad = rad*rad1/(rad+rad1);//1.0/(1.0/R1 + 1.0/R2); //average Radius of the sphere
              const double Rsqr = Rad*Rad;
              //const double Rcub = Rsqr*Rad;

              const double asqr = Rad*(-identdepth);
              const double a = sqrt(asqr); //contact radius, taken from identation depth via sqrt(R*identdepth) or identdepth^0.498 * R^(0.503-3.97e-6*indetdepth),see ref Lin et al Spherical identation of soft matter

              // we could also try to measure contact radius directly as it is done in isr2d
              const double acub = a*asqr;
              //const double afou = asqr*asqr;
              const double afif = acub*asqr;
              //const double znam = (4.0*a-3.0*M_PI*Rad)*(4.0*a-3.0*M_PI*Rad);
              //const double simpleznam = (3.0*M_PI*Rad)*(3.0*M_PI*Rad);
              //const double firstpart = (16.0*afif - 36.0*M_PI*Rad*afou + 27.0*M_PI*M_PI*Rsqr*acub) / znam ;
               const double B = 0.0282385035;//1.0/8.3439;//0.5*19.59; //kPa  //B in terms of young, looking for efficiency // B=E/6/(1-nu*nu)
             // const double B=0.5*19.59;
              //frad_Neo = - 8.0*B * firstpart/ 3.0 / Rad ;

              double hertz_coeff = hertz_x - (hertz_x-hertz_c)*cosfi;
              //if ((mytype==EEL)&&(nbtype==EEL)) hertz_coeff*=2.5; //mimicing EEL's higher stiffness

              frad = - hertz_coeff*8.0*B*acub / Rad;
              Urad  = Urad + (hertz_coeff*16.0*B*afif / Rsqr / 5.0);
              //dfrad=((80.0*acub-144.0*M_PI*Rad*asqr+81.0*M_PI*M_PI*Rsqr*a)/znam
                      //-8.0*(16.0*afou-36.0*M_PI*Rad*acub+27.0*M_PI*M_PI*Rsqr*asqr)/(4.0*a-3.0*M_PI*Rad)/znam)*4.0*B/3.0;
          }
          else {frad=0.;}
          //

          //frad*=0.2/pow(popravka,2.5);
          //frad*=0.2;        

          double fwmlc=(zz<0.99)?(kbT/lmax/popravka/popravka/popravka*(6.0*zz+(3.0*zz*zz-2.0*zz*zz*zz)/(1-zz)/(1-zz))):(kbT*10000.0/lmax/popravka/popravka/popravka);
           //double fwmlc;
           //if (zz<0.99)  fwmlc=kbT/lmax/popravka*(6.0*zz+(3.0*zz*zz-2.0*zz*zz*zz)/(1-zz)/(1-zz));
                               //else  fwmlc=kbT/lmax/popravka*(2000000.0*(zz-0.99)+10000.0);

          if (zz<0.99) Urad = Urad + kbT*(3.0*zz*zz-2.0*zz*zz*zz)/(1.0-zz)/popravka/popravka;
          else Urad =Urad + (kbT*10000.0*zz)/popravka/popravka;
              //Urad=Urad + kbT*(2.0e6*((zz-0.99)*(zz-0.99))/2.0+10000.0*(zz-0.99)+100.0);

          //const double dfwmlc=kbT/lmax/lmax*(6.0+(6.0*zz-6*zz*zz)/(1.0-zz)/(1.0-zz)+(6*zz*zz-4*zz*zz*zz)/(1.0-zz)/(1.0-zz)/(1.0-zz));

          frad+=fwmlc;
          //if ((mytype==EEL)&&(nbtype==EEL)) frad+=fwmlc;

          //const double df=dfrad*0.2+dfwmlc/(popravka*popravka*popravka);

          //end of wmlc+hertz model    */

        //x-,y-, z-projections of force
        double deltafx = frad*invr*(x1-x);
                  //if ((mytype==EEL)&&(nbtype==EEL)) deltafx+=1e-5;
        double deltafy = frad*invr*(y1-y);
        double deltafz = frad*invr*(z1-z);
        fx+= deltafx; fmx+= -deltafx;
        fy+= deltafy; fmy+= -deltafy;
        fz+= deltafz; fmz+= -deltafz;

        sx+=frad*invr*abs(x1-x);
        smx+=frad*invr*abs(x1-x);
        sy+=frad*invr*abs(y-y1);
        smy+=frad*invr*abs(y-y1);
        sz+=frad*invr*abs(z-z1);
        smz+=frad*invr*abs(z-z1);

        /*fxx+=(invr*invr*(x1-x)*(x1-x)*(dfrad-frad*invr)+frad*invr);
        fxy+=invr*invr*(y1-y)*(x1-x)*(dfrad-frad*invr);
        fxz+=invr*invr*(z1-z)*(x1-x)*(dfrad-frad*invr);
        fyy+=(invr*invr*(y1-y)*(y1-y)*(dfrad-frad*invr)+frad*invr);
        fyz+=invr*invr*(y1-y)*(z1-z)*(dfrad-frad*invr);
        fzz+=(invr*invr*(z1-z)*(z1-z)*(dfrad-frad*invr)+frad*invr);*/

        // take into account higher circumfer stiffness in tunica media according to Holzapfel

        thatcell->set_forces(fmx,fmy,fmz);
#pragma omp flush
        thatcell->set_sn(smx,smy,smz);
#pragma omp flush

        }   //end of neighbour cycle

        thiscell->set_forces(fx,fy,fz);
#pragma omp flush
        thiscell->set_sn(sx,sy,sz);
#pragma omp flush
        //cell_array[i].set_Hessian(fxx,fxy,fxz,fyy,fyz,fzz);

    }  //end of cell cycle
#pragma omp critical
    {
        globalU+=Urad;

    }

    } // end of parallel section

    return (globalU);
}


double tissue::DoStep(double timestep)
{
    double b=1; //damphing coefficient
    double err=0.;

    double maxforce = set_all_forces(); //detect forces between this cell and all cells
    //cout << "maxforce= "<<maxforce<<endl;

   // timestep=min(timestep,tol*b/maxforce);

    double inv=timestep/b;

    int numcells = cell_array.size();
//#ifdef OMP
//omp_set_num_threads(2);
//#endif
#pragma omp parallel
    {
    double tmp=0.,err_local=0.,fx,fy,fz,x=0,y=0,z=0,deltax,deltay,deltaz,sinphi,cosphi,frad,ffi;
    bool b0,b1,b2;

#pragma omp for
    for (int i=0;i<numcells;i++)
    {

        cell_array[i].GetMobility(b0,b1,b2);
        cell_array[i].get_forces(fx,fy,fz);  //get forces in current cell

        if (!axiflag){              //CARTESIAN GEOMETRY
        deltax=b0*inv*fx;
        deltay=b1*inv*fy;
        deltaz=b2*inv*fz;
        }
        else{                       //CYLINDRICAL GEOMETRY
            cell_array[i].getpos(x,y,z);
            sinphi=y/sqrt(y*y+z*z);
            cosphi=z/sqrt(y*y+z*z);
            deltax=b2*inv*fx;
            frad= fy*sinphi+fz*cosphi;
            ffi = fy*cosphi-fz*sinphi;
            deltay=inv*(b0*frad*sinphi+b1*ffi*cosphi);
            deltaz=inv*(b0*frad*cosphi-b1*ffi*sinphi);
        }
        cell_array[i].movpos(deltax,deltay,deltaz);

        tmp = sqrt(deltax*deltax+deltay*deltay+deltaz*deltaz);
        if(tmp>err_local) err_local=tmp;
    }
#pragma omp critical
    {
        if(err_local>err) err=err_local;
    }
    } //end of parallel section

    return(b*err/timestep);

}

/*double tissue::DoExpStep(double timestep)
{
    double b=1; //damphing coefficient
    double err=0., tol=0.030/2.0;

    double maxforce = set_all_forces(); //detect forces between this cell and all cells
    //cout << "maxforce= "<<maxforce<<endl;

   // timestep=min(timestep,tol*b/maxforce);

    double inv=timestep/b;

    int numcells = cell_array.size();
//#ifdef OMP
omp_set_num_threads(2);
//#endif
#pragma omp parallel
    {
    double tmp=0.,err_local=0.,fx,fy,fz,x,y,z,Ax,Ay,Az,newx,newy,newz,sinphi,cosphi,frad,ffi;
    bool b0,b1,b2;

#pragma omp for
    for (int i=0;i<numcells;i++)
    {

        cell_array[i].GetMobility(b0,b1,b2);
        cell_array[i].getpos(x,y,z);
        cell_array[i].get_forces(fx,fy,fz);  //get forces in current cell
        cell_array[i].get_linearized_forces(Ax,Ay,Az);

        const double Nx = fx-Ax*x;
        const double Ny = fy-Ay*y;
        const double Nz = fz-Az*z;

       // cout << "fx=="<<fx<<endl;

        if (!axiflag){              //CARTESIAN GEOMETRY
        newx=(b0)?(exp(timestep*Ax)*x+timestep*fi1(timestep*Ax)*Nx):x;
        newy=(b1)?(exp(timestep*Ay)*y+timestep*fi1(timestep*Ay)*Ny):y;
        newz=(b2)?(exp(timestep*Az)*z+timestep*fi1(timestep*Az)*Nz):z;
        }
        else{                       //CYLINDRICAL GEOMETRY
            //sinphi=y/sqrt(y*y+z*z);
            //cosphi=z/sqrt(y*y+z*z);
            //deltax=b2*inv*fx;
            //frad= fy*sinphi+fz*cosphi;
            //ffi = fy*cosphi-fz*sinphi;
            //deltay=inv*(b0*frad*sinphi+b1*ffi*cosphi);
            //deltaz=inv*(b0*frad*cosphi-b1*ffi*sinphi);
        }
        cell_array[i].setpos(newx,newy,newz);

        tmp = sqrt((newx-x)*(newx-x)+(newy-y)*(newy-y)+(newz-z)*(newz-z));
        if(tmp>err_local) err_local=tmp;
    }
#pragma omp critical
    {
        if(err_local>err) err=err_local;
    }
    } //end of parallel section

    return(b*err/timestep);

}*/

double tissue::DoCGStep(double *s, double* fold, double & bettaPR, double & sumold, const int method)
{
    double bettaHS,bettaDY,err=0;
    double globalU = set_all_forces(); //detect forces between this cell and all cells
    int numcells = cell_array.size();

//#ifdef OMP
//omp_set_num_threads(2);
//#endif
//#pragma omp parallel
    //{
    double tmp, sum=0,sum2=0,err_local=0.,fx,fy,fz,x,y,z,sinphi,cosphi,frad,ffi;
    bool b0,b1,b2;

//#pragma omp for
    for (int i=0;i<numcells;i++)
    {
        cell_array[i].get_forces(fx,fy,fz);  //get forces in current cell
        cell_array[i].GetMobility(b0,b1,b2);

        if (! axiflag) tmp=fx*fx*b0+fy*fy*b1+fz*fz*b2;
        else {cell_array[i].getpos(x,y,z);
              sinphi=y/sqrt(y*y+z*z);
              cosphi=z/sqrt(y*y+z*z);
            frad= fy*sinphi+fz*cosphi;
            ffi = fy*cosphi-fz*sinphi;
            tmp=fx*fx*b2+frad*frad*b0+ffi*ffi*b1;}
        sum+=tmp;
        if (sqrt(tmp)>err_local) err_local=sqrt(tmp);

       int ind=i*3;
       if (bettaPR==0) {                         //first iteration
           s[ind] = fx; s[ind+1] = fy; s[ind+2]= fz;
       }
       else {
           //sum3+=s[ind]*(fx - fold[ind])*b0+s[ind+1]*(fy - fold[ind+1])*b1+s[ind+2]*(fz - fold[ind+2])*b2;
           if (method==1) bettaPR=0;
           s[ind] = fx + bettaPR*s[ind];
           s[ind+1] = fy + bettaPR*s[ind+1];
           s[ind+2]= fz + bettaPR*s[ind+2];
           if (! axiflag) sum2+= fx*(fx - fold[ind])*b0+fy*(fy - fold[ind+1])*b1+fz*(fz - fold[ind+2])*b2;
           else { const double foldrad= fold[ind+1]*sinphi+fold[ind+2]*cosphi;
               const double foldfi = fold[ind+1]*cosphi-fold[ind+2]*sinphi;
               sum2+= fx*(fx - fold[ind])*b2 +frad*(frad-foldrad)*b0+ffi*(ffi-foldfi)*b1;}

       }

       fold[ind]=fx; fold[ind+1]=fy; fold[ind+2]=fz;
    }


    bettaPR=(bettaPR==0)?(sum/sumold):(sum2/sumold);
    if (bettaPR<0) bettaPR=0;
    //bettaHS=(betta==0)?(sum/sumold):(sum2/sum3);
    //if (bettaHS<0) bettaHS=0;
    //bettaDY=(betta==0)?(sum/sumold):(sum/sum3);

    //betta=sum/sumold;
    sumold=sum;
    //err_local=sqrt(sum)/numcells;

     Linesearch(s,2e-3);
     //Testsearch(s,1e-3);

//#pragma omp critical
    {
        if(err_local>err) err=err_local;
    }
    //} //end of parallel section

    return(err);

}

void tissue::CheckConnectivities()
{
    int numcells=cell_array.size();
    double x,y,z,x1,y1,z1,rad,rad1;
    int n;

    for (int i=0;i<numcells;i++)
    {
        n=0;
        cell me = cell_array[i];
        me.getpos(x,y,z);
        rad = me.getrad();

        for(int j=i+1;j<numcells;j++)
        {
            cell nb = cell_array[j];
            nb.getpos(x1,y1,z1);
            rad1 = nb.getrad();
            const double dist = sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1)+(z-z1)*(z-z1));
            if (dist==0) continue;
            const double identdepth = dist-rad-rad1;
            if((identdepth<0)||(identdepth<DTOL)) { // only <DTOL
                cell_array[i].num_nb++;
                int nbi=cell_array[i].num_nb;
                cell_array[j].num_nb++;
                int nbj=cell_array[j].num_nb;
                cell_array[i].nbrs[nbi-1]=j;
                cell_array[j].nbrs[nbj-1]=i;

                // Store springs directions
                const double cosfi = abs(z1-z)/dist; //angle between spring and circumferential direction
                cell_array[i].springdir[nbi-1] = cosfi;
                cell_array[j].springdir[nbj-1] = cosfi;

            }
        }
    }
}

void tissue::AddZPeriodicBC(double coord1, double coord2)
{
    assert (!(coord1==coord2));
    int n1,n2,ind1,ind2,lpairity;
    char normaldir='Z';
    double x1,y1,z1,x2,y2,z2,dist;
    std::vector<int> boundary1,boundary11, boundary2,boundary22;
    this->SelectBoundary(normaldir,coord1,boundary1);
    this->SelectBoundary(normaldir,coord1+mean_rad,boundary11);
    this->SelectBoundary(normaldir,coord2,boundary2);
    this->SelectBoundary(normaldir,coord2-mean_rad,boundary22);

    for (lpairity=1;lpairity<=2;lpairity++)
{
    n1=(lpairity==1)?boundary1.size():boundary11.size();
    double newz=(lpairity==1)?(coord2+mean_rad):(coord2+2*mean_rad);
    n2=boundary22.size()+boundary2.size();

    for (int i=0;i<n1;i++) {

        ind1=(lpairity==1)?boundary1[i]:boundary11[i];

        cell_array[ind1].getpos(x1,y1,z1);

        for (int j=0;j<n2;j++){

            ind2=(j<boundary22.size())?boundary22[j]:boundary2[j-boundary22.size()];
            cell_array[ind2].getpos(x2,y2,z2);
            dist=sqrt((newz-z2)*(newz-z2)+(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
            const double identdepth = dist-2*mean_rad;
        if ((identdepth<0)||(identdepth<DTOL)) {
            cell_array[ind1].num_nb++;
            int nbind1 = cell_array[ind1].num_nb;
            cell_array[ind2].num_nb++;
            int nbind2 = cell_array[ind2].num_nb;
            cell_array[ind1].nbrs[nbind1-1]=ind2;
            cell_array[ind2].nbrs[nbind2-1]=ind1;
        }
        }
    }
    }
}

void tissue::SelectBoundary(char locdir, double locvalue, std::vector<int> & selection)
{
    int it;
    double x,y,z,dist, rad;
    int numcells=cell_array.size();
    selection.clear();

  for(it = 0; it<numcells; it++)
  {
      cell_array[it].getpos(x,y,z);
      rad = cell_array[it].getrad();
      //double loc=(locvalue>0)?(locvalue-rad):(locvalue+rad);
      if(locdir=='X') dist=abs(x-locvalue);
      else if (locdir=='Y') dist=abs(y-locvalue);
      else if (locdir=='Z') dist=abs(z-locvalue);
      else if (locdir=='R') dist=abs(sqrt(y*y+z*z)-locvalue);
      else {std::cout<<"Input data error"<<std::endl; return;};

      if (dist<DTOL)
      {
          selection.push_back(it);
      }
  }
}

void tissue::MoveBoundary(const std::vector<int> & selection, double *v)
{
    double sinphi,cosphi,x0,y0,z0,v0=v[0],v1=v[1],v2=v[2];
    int size=selection.size();

    if (!axiflag)    {          //CARTESIAN GEOMETRY

    for(int it = 0; it<size; it++)
    {
        int i=selection[it];
        cell_array[i].movpos(v0,v1,v2);

    }
    } // endif for cube geometry
    else { for(int it = 0; it<size; it++)   //CYLINDRICAL GEOMETRY
        {
            int i=selection[it];
            cell_array[i].getpos(x0,y0,z0);
            //cell_array[i].GetMobility(mob00,mob01,mob02);
            //x=x0+v2;
            sinphi=y0/sqrt(z0*z0+y0*y0);
            cosphi=z0/sqrt(z0*z0+y0*y0);
            //y = y0 + v0*sinphi + v1*sqrt(y0*y0+z0*z0)*cosphi;
            //z = z0 + v0*cosphi - v1*sqrt(y0*y0+z0*z0)*sinphi;
            //cell_array[i].movpos(v2,v0*sinphi + v1*sqrt(y0*y0+z0*z0)*cosphi,v0*cosphi - v1*sqrt(y0*y0+z0*z0)*sinphi);
            cell_array[i].movpos(v2,v0*sinphi + v1*cosphi,v0*cosphi - v1*sinphi);
            // cell_array[i].SetMobility(mob0&&mob00,mob1&&mob01,mob2&&mob02);   //BUG?
        }
    }
}

void tissue::FixBoundary(const std::vector<int> & selection, bool *mobility)
{
    bool b0=mobility[0], b1=mobility[1], b2=mobility[2];

    int size=selection.size();

    for(int it = 0; it<size; it++)
    {
        bool b00, b11, b22;
        cell_array[selection[it]].GetMobility(b00,b11,b22);
        cell_array[selection[it]].SetMobility(b0&&b00,b1&&b11,b2&b22);
    }
}

void tissue::ApplyForceToBoundary(const std::vector<int> & selection, double *forces)
{
    double sinphi,cosphi,x0,y0,z0,y,z,v0=forces[0],v1=forces[1],v2=forces[2];
    int size=selection.size();

    if (!axiflag)    {          //CARTESIAN GEOMETRY

    for(int it = 0; it<size; it++)
    {
        int i=selection[it];
        cell_array[i].apply_external_forces(v0,v1,v2);
    }
    } // endif for cube geometry
    else { for(int it = 0; it<size; it++)   //CYLINDRICAL GEOMETRY
        {
            int i=selection[it];
            cell_array[i].getpos(x0,y0,z0);
            sinphi=y0/sqrt(z0*z0+y0*y0);
            cosphi=z0/sqrt(z0*z0+y0*y0);
            double fx=v2;
            double fy = v0*sinphi + v1*cosphi;
            double fz = v0*cosphi - v1*sinphi;
            cell_array[i].apply_external_forces(fx,fy,fz);
            // cell_array[i].SetMobility(mob0&&mob00,mob1&&mob01,mob2&&mob02);   //BUG?
        }
    }
}

void tissue::Set_initial_condition(double *v)
{
    int numcells=cell_array.size();

    if (!axiflag)    {          //CARTESIAN GEOMETRY

    for(int i = 0; i<numcells; i++)
    {
        bool mob00,mob01,mob02;
        double x,y,z;
        cell_array[i].GetMobility(mob00,mob01,mob02);
        cell_array[i].getpos(x,y,z);
        cell_array[i].setpos(x*v[0]*mob00,y*v[1]*mob01,z*v[2]*mob02);
        //cell_array[i].setpos(x,y,z);
        //cell_array[i].SetMobility(mob0&&mob00,mob1&&mob01,mob2&&mob02);   //BUG???
    }
    } // endif for cube geometry
}

void tissue::GetBoundaryResponse(const std::vector<int> & selection, double & frx, double & fry, double & frz)
{
    {

        double ftx, fty, ftz, frad, ffi,x,y,z,sinphi,cosphi;
        frx=fry=frz=0;

        int size=selection.size();

        for(int it = 0; it<size; it++)
        {
            cell_array[selection[it]].get_forces(ftx,fty,ftz);

            if (!axiflag) {frx+=ftx; fry+=fty; frz+=ftz;} //CARTESIAN GEOMETRY
            else {cell_array[selection[it]].getpos(x,y,z);
                sinphi=y/sqrt(y*y+z*z);                 //CYLINDRICAL GEOMETRY
                cosphi=z/sqrt(y*y+z*z);
                frad= fty*sinphi+ftz*cosphi;
                ffi = fty*cosphi-ftz*sinphi;
                frx+=frad; fry+=ffi; frz+=ftx;}

        }
    }

}

void tissue::FileOutput(string filename)
{

    double x=0, y=0, z=0;
    ofstream myFile(filename, ios_base::out);
    if (myFile.is_open())
    {
    cout<<"write in file"<<endl;
    myFile<<setprecision(10);

    if (!axiflag)
    myFile <<  "TYPE,   X      ,       Y      ,       Z      ,     RAD      ,"<<
               "     SX      ,     SY      ,     SZ       "<<endl;
    else myFile <<  "TYPE,   X      ,       Y      ,       Z      ,      RAD      ," <<
                    "     SX      ,     SR      ,     SFI      "<<endl;


    for (std::vector<cell>::iterator it = cell_array.begin() ; it != cell_array.end(); ++it)
    {
    it->getpos(x,y,z);
    double rad=it->getrad();
    double sx,sy,sz;
    it->get_sn(sx,sy,sz);
    //int nb = it->num_nb;
    if (!axiflag)
    myFile << fixed << (it->gettype())<<" , "<< x << " , "<<y<<" , " << z <<" , "<< rad << " , "<<sx <<
              " , "<< sy <<" , "<< sz << endl;
    else
    {   const double sinphi=z/sqrt(y*y+z*z);
        const double cosphi=y/sqrt(y*y+z*z);
        const double srad= sy*abs(cosphi) - sz*abs(sinphi);
        const double sfi = sy*abs(sinphi) + sz*abs(cosphi);
        myFile << fixed << (it->gettype())<<" , " <<x << " , "<<y<<" , " << z <<" , "<< rad << " , "<< sx <<
                  " , "<< srad <<" , "<< sfi << endl;}
    }

    myFile.close();
    }

}

void tissue::FileInput(string filename)
{

    double x, y, z, rad, tmp;
    char s;
    int id;
    ifstream myFile(filename, ios_base::in);

    if (myFile.is_open())
    {
    string headings;
    getline(myFile, headings);

    int i=0;
    while (!myFile.eof())
    {
    myFile>> id >> s >>x >> s>>y >> s>>z >>s>> rad>>s>>tmp>>s>>tmp>>s>>tmp;
    cell_array[i].setpos(x,y,z);
    cell_array[i].setrad(rad);
    cell_array[i].settype((celltype)id);
    i++;
    }

    myFile.close();
    }

}

void tissue::Linesearch(double *s, const double TOL)
{
    const int NITMAX=1000000;
    double deltax, deltay, deltaz, alpha=0,deltaalpha;
    int numcells=cell_array.size();
    int ind;

/*   // QUADRATIC SEARCH
    double back=0, h=20;
    bool b0,b1,b2;
    double LTOL=1e-14;

    for (int it=0;it<NITMAX;it++)
    {

    double globalUmid=set_all_forces();
            for (int i=0;i<numcells;i++)
            {
            cell_array[i].GetMobility(b0,b1,b2);
            ind=i*3;

            deltax=b0*(back+h)*s[ind];
            deltay=b1*(back+h)*s[ind+1];
            deltaz=b2*(back+h)*s[ind+2];
            cell_array[i].movpos(deltax,deltay,deltaz);
            }
    double globalU2=set_all_forces();
            for (int i=0;i<numcells;i++)
            {
            cell_array[i].GetMobility(b0,b1,b2);
            ind=i*3;

            deltax=-b0*(back-2*h)*s[ind];
            deltay=-b1*(back-2*h)*s[ind+1];
            deltaz=-b2*(back-2*h)*s[ind+2];
            cell_array[i].movpos(deltax,deltay,deltaz);
            }
    double globalU1=set_all_forces();

        deltaalpha=-h/2.0*(globalU2-globalU1)/(globalU2-globalUmid+globalU1);
        if ((globalU2-globalUmid+globalU1)<0){h*=-5;continue;}
        h=deltaalpha/5;

        if (abs(deltaalpha)<LTOL) {cout<< "LINESEARCH DONE IN "<<it<<" iterations"<<endl;
                                  return;}

        for (int i=0;i<numcells;i++)
        {
            bool b0,b1,b2;
            cell_array[i].GetMobility(b0,b1,b2);
            ind=i*3;
            deltax=b0*(back+h+deltaalpha)*s[ind];
            deltay=b1*(back+h+deltaalpha)*s[ind+1];
            deltaz=b2*(back+h+deltaalpha)*s[ind+2];
            cell_array[i].movpos(deltax,deltay,deltaz);
        }

          double globalUmidnew=set_all_forces();
      if (globalUmidnew>globalUmid) {back=-h-deltaalpha;h=deltaalpha/2;}

        }
        cout << "ERROR !! COULD NOT REACH EQUILLIBRIUM"<<endl;
        return;} */

     // GOLD SECTION

    const double tau=0.618034;
    const double _tau_=1-tau;
    const double LMARGIN=0., RMARGIN=90.;
    double X[2],globalU[2];

    double XL=LMARGIN;
    double XR=RMARGIN;

    double tostart=0;

    for (int it=0;it<NITMAX;it++)
    {

    X[0]=XL + _tau_*(XR-XL);
    X[1]=XL + tau*(XR-XL);


    for (int igold=0;igold<2;igold++)
    {
    double coeff=tostart + X[igold];

    for (int i=0;i<numcells;i++)
    {   ind=i*3;
        bool b0,b1,b2;
        double x,y,z;
        cell_array[i].GetMobility(b0,b1,b2);
        ind=i*3;


        if (!axiflag){              //CARTESIAN GEOMETRY
        deltax=b0*coeff*s[ind];
        deltay=b1*coeff*s[ind+1];
        deltaz=b2*coeff*s[ind+2];
        }
        else{                       //CYLINDRICAL GEOMETRY
            cell_array[i].getpos(x,y,z);
            const double sinphi=y/sqrt(y*y+z*z);
            const double cosphi=z/sqrt(y*y+z*z);
            deltax=b2*coeff*s[ind];
            const double frad= s[ind+1]*sinphi+s[ind+2]*cosphi;
            const double ffi = s[ind+1]*cosphi-s[ind+2]*sinphi;
            deltay=coeff*(b0*frad*sinphi+b1*ffi*cosphi);
            deltaz=coeff*(b0*frad*cosphi-b1*ffi*sinphi);
        }


        cell_array[i].movpos(deltax,deltay,deltaz);

    }

    globalU[igold]=set_all_forces();
    tostart=-X[igold];

    }

    if (globalU[0]<globalU[1]) XR=X[1];
    else XL=X[0];

//    tostart=X[1]-XL;

    if ((abs(X[1]-XL)/(X[1]+TOL))<TOL) {
        //if (abs(X[1]-LMARGIN)/(X[1]+TOL)<TOL) cout << "WARNING !! LINEAR SEARCH MINIMUM AT LEFT MARGIN"<<endl;
        if (abs(X[1]-RMARGIN)/(X[1]+TOL)<TOL) cout << "WARNING !! LINEAR SEARCH MINIMUM AT RIGHT MARGIN"<<endl;
        //cout<< "LINESEARCH DONE IN "<<it<<" iterations"<<endl;

        return;}

    }
    cout << "LINESEARCH ERROR !! COULD NOT FIND MINIMUM"<<endl;
    return;

}  // endof goldsection
void tissue::Testsearch(double *s, const double TOL)
{
    const int NITMAX=1000000;
    double deltax, deltay, deltaz, alpha=0,deltaalpha=0;
    double fx,fy,fz,fxx,fxy,fxz,fyy,fyz,fzz,err;
    int numcells=cell_array.size();
    int ind;

    for (int it=0;it<NITMAX;it++)
    {
    double dUds=0, d2Uds2=0;
    double globalU=set_all_forces();

    for (int i=0;i<numcells;i++)
    {   ind=i*3;
        bool b0,b1,b2; cell_array[i].GetMobility(b0,b1,b2);
        cell_array[i].get_forces(fx,fy,fz);
        cell_array[i].get_Hessian(fxx,fxy,fxz,fyy,fyz,fzz);

        dUds+=-fx*s[ind]*b0-fy*s[ind+1]*b1-fz*s[ind+2]*b2;
        d2Uds2+=-2.0*(-fxx*s[ind]*s[ind]*b0 -2*fxy*s[ind]*s[ind+1]*b0*b1 - 2*fxz*s[ind]*s[ind+2]*b0*b2 -
                - fyy*s[ind+1]*s[ind+1]*b1 - 2*fyz*s[ind+1]*s[ind+2]*b1*b2 -fzz*s[ind+2]*s[ind+2]*b2); //gradient*(search direction)
    }

    //cout<< "alpha="<<alpha<<endl;
    alpha+=-1*dUds/d2Uds2;
    deltaalpha=-1*dUds/d2Uds2;
    err= abs(deltaalpha/alpha);

    for (int i=0;i<numcells;i++)
    {
        bool b0,b1,b2;
        cell_array[i].GetMobility(b0,b1,b2);
        ind=i*3;
        deltax=b0*deltaalpha*s[ind];
        deltay=b1*deltaalpha*s[ind+1];
        deltaz=b2*deltaalpha*s[ind+2];
        cell_array[i].movpos(deltax,deltay,deltaz);
    }

    if (err<TOL)   {double globalU=set_all_forces();
        //cout<< "TESTSEARCH DONE IN "<<it<<" iterations"<<endl;
        //cout << "alpha ="<<alpha<<endl;
        return;}

    }
    cout << "ERROR !! LINESEARCH COULD NOT REACH EQUILLIBRIUM"<<endl;
    return;

}

double tissue::DoRK4Step(double timestep)       //REM: b should be added to class tissue
{
    double b=1; //damphing coefficient
    double err=0., tol=0.030/2.0;

    double maxforce = set_all_forces(); //detect forces between this cell and all cells
    tissue tmp=*this;

    //cout << "maxforce= "<<maxforce<<endl;

    double inv=timestep/b;
    int numcells = cell_array.size();
    double *deltaxglobal=new double [numcells];//
    double *deltayglobal=new double [numcells];
    double *deltazglobal=new double [numcells];

//#ifdef OMP
//omp_set_num_threads(2);
//#endif
#pragma omp parallel
    {
    double tmperr=0.,err_local=0.,fx,fy,fz,x,y,z,deltax,deltay,deltaz,sinphi,cosphi,frad,ffi;
    bool b0,b1,b2;


    for (int istage=1;istage<=4;istage++)
    {
    #pragma omp for
    for (int i=0;i<numcells;i++)
    {

        cell_array[i].GetMobility(b0,b1,b2);
        tmp.cell_array[i].get_forces(fx,fy,fz);  //get forces in current cell
        cell_array[i].getpos(x,y,z);

        if (!axiflag){              //CARTESIAN GEOMETRY
            //k1=f/b
        if (istage==1) {deltaxglobal[i]=deltayglobal[i]=deltazglobal[i]=0;}
        deltaxglobal[i]+=((istage==1)||(istage==4))?(inv/6.0*fx*b0):(inv/3.0*fx*b0);
        deltayglobal[i]+=((istage==1)||(istage==4))?(inv/6.0*fy*b1):(inv/3.0*fy*b1);
        deltazglobal[i]+=((istage==1)||(istage==4))?(inv/6.0*fz*b2):(inv/3.0*fz*b2);
        }
        else{                       //CYLINDRICAL GEOMETRY
            //sinphi=y/sqrt(y*y+z*z);
            //cosphi=z/sqrt(y*y+z*z);
            //deltax=b2*inv*fx;
            //frad= fy*sinphi+fz*cosphi;
            //ffi = fy*cosphi-fz*sinphi;
            //deltay=inv*(b0*frad*sinphi+b1*ffi*cosphi);
            //deltaz=inv*(b0*frad*cosphi-b1*ffi*sinphi);
        }
        //cell_array[i].GetMobility(b0,b1,b2);
        if (istage==4)
        {
            cell_array[i].setpos(x+deltaxglobal[i],y+deltayglobal[i],z+deltazglobal[i]);
            tmperr = sqrt(deltaxglobal[i]*deltaxglobal[i]+deltayglobal[i]*deltayglobal[i]+deltazglobal[i]*deltazglobal[i]);
            if(tmperr>err_local) err_local=tmperr;
         }

    else {
            if ((istage==1)||(istage==2)) {deltax=inv*fx/2.0; deltay=inv*fy/2.0; deltaz=inv*fz/2.0;}
            else if (istage==3) {deltax=inv*fx; deltay=inv*fy; deltaz=inv*fz;}
            tmp.cell_array[i].setpos(x+deltax*b0,y+deltay*b1,z+deltaz*b2);
         }
        }
        if (!(istage==4)) double t=tmp.set_all_forces();
    }



#pragma omp critical
    {
        if(err_local>err) err=err_local;
    }
    } //end of parallel section

delete (deltaxglobal,deltayglobal,deltazglobal);
return(maxforce);
}


tissue::~tissue() {}


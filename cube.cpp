#include "cube.h"

cube::cube(){}

cube::cube (double rad, double lX, double lY, double lZ)
{

   axiflag=0;
   mean_rad=rad;

    //spacings
    const double dZ = 2*mean_rad;
    const double dY = mean_rad*sqrt(3.0);        // "row spacing"
    const double dX = mean_rad*2*sqrt(2.0/3.0);  // "layer spacing"

    // number of cells in each direction
    //const int nI = 1 + (int)round(lX/dX);
    //const int nJ = 1 + (int)round(lY/dY);
    //const int nK = 1 + (int)round(lZ/dZ);

    const int nI = (int)floor(lX/dX);
    const int nJ = (int)floor(lY/dY);
    const int nK = (int)floor(lZ/dZ);


//    cout << (nJ-1)*dY << " ," << (nJ-1)*dY + 0.5*dZ/sqrt(3.0) << endl;


    int n = 0;
    double x, y, z;

    for (int i=0; i<nI; i++) {

        x = i*dX;
        const int layerParity = (i % 2);
        const double y0 = (layerParity == 0) ? 0.0 : 0.5*dZ/sqrt(3.0);

        for (int j=0; j<nJ; j++) {

            y = y0 + j*dY;
            const double z0 = (j % 2 == layerParity) ? 0.0 : 0.5*dZ;
            celltype type_id = (j==(nJ-1))? EEL : SMC; //upper layer contains EEL cells

            for (int k=0; k<nK; k++) {
                n++;
                z = z0 + k*dZ;
                tissue::cell_array.push_back(cell(type_id, mean_rad, x, y, z) );
            }

        }
    }

    std::cout << "Total number of cells is "<<tissue::cell_array.size()<<std::endl;
    this->CheckConnectivities();

}


cube::~cube()
{

}

